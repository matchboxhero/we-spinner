# Percentage Spinner
In a real world scenario I would be asking a lot of questions to get specifics about the component I'm building, the specification provided is vague which may or may not be part of the test.

## Tools
I'm using Parcel.js as the build tool (https://parceljs.org/)

JS is written in Vanilla JS and is heavily commented

CSS is written in SCSS and uses BEM notation and CSS custom properties to allow colours and spacing to be easily updated. The `$inner-stroke` and `$spinner-stroke` values can be updated to change how thick each line is. (making the `$spinner-stroke` too large will cause problems with the stroke animation)

HTML is vanilla HTML and pretty basic.

## About
The spinner is created using SVG and uses stroke-dasharray and stroke-dashoffset in order to increase the size of the inner spinner as the percentage increases.

The component features a 'start' button which is updated to 'pause' and 'resume' once the spinner has started and a 'cancel' button which will reset the component back to 0.

Upon completion the button and spinner change colour to green and the percentage count is replaced with the party popper emojii.

There's a CSS animation which can be uncommented from the main SCSS which I wrote but wasn't sure about.

I actually had a lot of fun building this and I hope it shows. I hope to hear from you soon!