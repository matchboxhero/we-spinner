const value = document.getElementById('js-percentage-value'); // Element the value will be updated
const stroke = document.getElementById('js-percentage-stroke'); // Element where the stroke will be updated
const button = document.getElementById('js-spinner-toggle'); // Start/Pause/Resume button element
const cancel = document.getElementById('js-cancel-button'); // Cancel button element
let percentage = 0; // Starting percentage
let animationStopped = true; // Start stop Bool

function updatePercentage() {

    if (animationStopped == true) {
        return; // If the stop variable is true and stop the function
    }

    setTimeout(function(e) { // Set timeout used to stagger the increase

        let percentage_converted = percentage / 100; // Convert to decimal
        let percentage_stroke = 351.858377202 * (1 - percentage_converted); // Work out the new value for the stroke dashoffset

        if(percentage < 100) { // If less than 100

            value.innerHTML = percentage + '<small>%</small>'; // Update the value
            stroke.style.strokeDashoffset = percentage_stroke; // Update the stroke-dashoffset

            updatePercentage(); // Run the function again

        }

        if (percentage == 100) { // If complete
            stroke.classList.add('percentage-radial__value--complete'); // Add complete class to stroke
            value.innerHTML = '🎉'; // Update value to party popper emojii
            button.innerHTML = 'Complete'; // Change button text to complete
            button.classList.add('button--success'); // Add success class to button
            cancel.innerHTML = 'Reset';
        }

        percentage++; // Add 1 to the percentage

    }, 33); // Timeout length can be updated to speed up or slow down the animation

}

button.addEventListener('click', function () { // On click of the Start/Pause/Resume button

    animationStopped = !animationStopped; // Toggle true/false animationStopped variable

    if (percentage < 100) { // If percentage is less than 100 when the button is clicked

        if (this.innerHTML == 'Start') { // If button text is 'start'
            this.innerHTML = 'Pause'; // Change button text to 'pause'
        } else if (this.innerHTML == 'Pause') { // If button text is 'pause'
            this.innerHTML = 'Resume'; // Change button text to 'resume'
        } else { // otherwise
            this.innerHTML = 'Pause'; // Change button text to 'pause'
        }

        updatePercentage(); // Run the update percentage function

        cancel.disabled = false; // Remove disabled attribute from the cancel button

    }

});

cancel.addEventListener('click', function() { // On click of the cancel button

    animationStopped = true; // Stop the animation the next time updatePercentage Runs
    percentage = 0; // Reset percentage to 0
    value.innerHTML = percentage + '<small>%</small>'; // update the value to the on screen to 0%
    button.innerHTML = 'Start'; // Update button text to say start
    stroke.style.strokeDashoffset = 351.858377202; // reset stroke-dashoffset
    cancel.disabled = true; // Disable the cancel button

    // If complete remove the extra classes and change the cancel button text back
    cancel.innerHTML = 'Cancel';
    button.classList.remove('button--success');
    stroke.classList.remove('percentage-radial__value--complete');

});